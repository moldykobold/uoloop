using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace UOLoop
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            
            button1.Enabled = false;
            comboBox1.Enabled = false;
            textBox1.Enabled = false;
            button2.Enabled = true;
            try
            {
                timer1.Interval = Int32.Parse(textBox1.Text) * 1000;
                timer1.Start();
                timer1_Tick(this, e);
            }
            catch (Exception error)
            {
                MessageBox.Show(error.ToString());
                System.Windows.Forms.Application.Exit();
            }

        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            [System.Runtime.InteropServices.DllImport("user32.dll")]
            static extern bool PostMessage(IntPtr hWnd, uint Msg, int wParam, int lParam);

            const uint WM_KEYDOWN = 0x0100;
            int VKEY = 0;

            Process p = Process.GetProcessesByName("client")[0];

            if (p != null)
            {
                IntPtr handle = p.MainWindowHandle;
                try
                {
                    switch (comboBox1.SelectedItem.ToString())
                    {
                        case "F1":
                            VKEY = 0x70;
                            break;
                        case "F2":
                            VKEY = 0x71;
                            break;
                        case "F3":
                            VKEY = 0x72;
                            break;
                        case "F4":
                            VKEY = 0x73;
                            break;
                        case "F5":
                            VKEY = 0x74;
                            break;
                        case "SPACE":
                            VKEY = 0x20;
                            break;
                        default:
                            MessageBox.Show("You must select a key from the drop down menu.");
                            timer1.Stop();
                            break;
                    }
                    PostMessage(handle, WM_KEYDOWN, VKEY, 1);
                }
                catch (Exception error)
                {
                    MessageBox.Show(error.ToString());
                    System.Windows.Forms.Application.Exit();
                }
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            timer1.Stop();
            button2.Enabled = false;
            button1.Enabled = true;
            comboBox1.Enabled = true;
            textBox1.Enabled = true;
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            comboBox1.SelectedIndex = 0;
            button2.Enabled = false;
        }

        private void label3_Click(object sender, EventArgs e)
        {
            MessageBox.Show("\"Copyright\" 2020\nCoded by /u/moldykobold", "UOLoop");
        }
    }
}